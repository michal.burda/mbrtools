test_that("impute", {
  expect_equal(impute(c(NA, 1:5, NA), method = 'mean'),
               c(3, 1:5, 3))
  expect_equal(impute(c(NA, 1:4, 8, NA), method = 'median'),
               c(3, 1:4, 8, 3))
  expect_equal(impute(factor(c('a', 'a', 'b', 'b', 'a', NA, NA)), method = 'mean'),
               factor(c('a', 'a', 'b', 'b', 'a', 'a', 'a')))
  expect_equal(impute(factor(c('a', 'a', 'b', 'b', 'a', NA, NA)), method = 'median'),
               factor(c('a', 'a', 'b', 'b', 'a', 'a', 'a')))
  
  d <- data.frame(a = c(NA, 1:5, NA),
                  b = c(NA, 1:4, 8, NA),
                  c = factor(c('a', 'a', 'b', 'b', 'a', NA, NA)),
                  d = c(T, T, NA, NA, F, F, F))
  
  e <- data.frame(a = c(3, 1:5, 3),
                  b = c(3, 1:4, 8, 3),
                  c = factor(c('a', 'a', 'b', 'b', 'a', 'a', 'a')),
                  d = c(T, T, F, F, F, F, F))
  
  expect_equal(impute(d, method = 'median'), e)
})
