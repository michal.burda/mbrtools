---
title: "${TITLE}"
author: "${USER}"
date: "`r format(Sys.time(), '%d. %m. %Y')`"
output:
    pdf_document:
      latex_engine: xelatex
    #word_document: default
---

```{r setup, include=FALSE}
library(tidyverse)
library(mbrtools)
library(knitr)
library(rmake)

knitr::opts_chunk$set(echo=FALSE)
options(knitr.kable.NA='')
pdf.options(encoding = 'CP1250')

input <- getParam('.depends')
data <- readRDS(input[1])
```


## Data Columns

```{r}
qualitySummary(data)
```

```{r, results='asis', fig.width=7, fig.height=3}
for (i in colnames(data)) {
  cat('\\pagebreak\n\n')
  cat('## ', i, '\n\n')
  cat(describe(data[[i]], name=i, plot=TRUE))
}
```
