library(tidyverse)
library(haven)
library(labelled)
library(rmake)

input <- getParam('.depends')
output <- getParam('.target')

raw <- read_spss(input[1]) %>%
  mutate(across(where(is.labelled), to_factor))

saveRDS(raw, output[1])
