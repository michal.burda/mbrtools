dir.create('incoming')

skel('data.R')
skel('qsummary.Rmd', skeleton='qsummary.Rmd')
skel('baseline.Rmd', skeleton='baseline.Rmd')

f <- file('Makefile.R')
writeLines(c('library(rmake)',
             '',
             "job <- c('incoming/data.csv' %>>% rRule('data.R') %>>% 'data.rds',",
             "         'data.rds' %>>% markdownRule('qsummary.Rmd') %>>% 'qsummary.pdf',",
             "         'data.rds' %>>% markdownRule('baseline.Rmd') %>>% 'baseline.pdf')",
             '',
             "makefile(job, 'Makefile')",
             ''),
           con = f)
close(f)

source('Makefile.R')
