#' @export
insert <- function(vec, x, after=length(vec)) {
  if (after <= 0) {
    return(c(x, vec))
  }
  c(vec[seq_len(after)], x, vec[-seq_len(after)])
}