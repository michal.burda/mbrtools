#' @export
formatNumber <- function(x, digits=2, na='NA') {
  result <- format(round(x, digits=digits), nsmall=digits, scientific=FALSE, trim=TRUE)
  result[is.na(x)] <- na
  result
}