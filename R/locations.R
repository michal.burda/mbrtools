#' @export
locations <- function(x, values, n=3, sep=', ') {
    xx <- x[!is.na(x)]
    ux <- unique(xx)
    tab <- tabulate(match(xx, ux))
    names(tab) <- as.character(ux)
    pos <- vapply(values, function(v) {
      ind <- which(!is.na(x) & x == v)
      ddd <- ifelse (length(ind) > n, ', ...', '')
      res <- paste0(head(ind, n=n), collapse=', ')
      paste0(res, ddd)
    }, character(1))
    paste0('"', values, '" @ ', pos, ' (', tab[as.character(values)], ')', collapse=sep)
}
