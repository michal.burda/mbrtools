#' @export
qqplot <- function(x) {
  d <- data.frame(x=scale(na.omit(x)))
  ggplot(d, aes(sample=x)) + 
    stat_qq() +
    geom_abline(slope=1)
}
