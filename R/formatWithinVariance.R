#' 
#' @return 
#' @author Michal Burda
#' @export
formatWithinVariance <- function(fit, digits=2) {
  d <- as.data.frame(VarCorr(fit))
  d <- d[is.na(d$var2), ]
  value <- d$vcov[nrow(d)]
  vsum <- sum(d$vcov)
  
  paste0(formatNumber(value, digits),
         ' (', 
         formatPercent(value / vsum),
         ' of total variance)')
}