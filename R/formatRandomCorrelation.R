#' Returns the correlation coefficient between random effects
#' @export
#' @importFrom lme4 VarCorr
formatRandomCorrelation <- function(fit, var1='(Intercept)', var2, digits=2) {
  d <- as.data.frame(VarCorr(fit))
  v <- d[!is.na(d$var1) & !is.na(d$var2) & d$var1 == var1 & d$var2 == var2, 'sdcor']
  stopifnot(length(v) == 1)
  formatNumber(v, digits)
}
