#' @export
formatPluralCZ <- function(x, word, defs) {
  assert_that(is.number(x))
  assert_that(is.string(word))
  
  x <- as.integer(x)
  
  if (x >= 11 && x <= 20) {
    i <- 3
  } else {
    x <- as.character(x)
    x <- substr(x, nchar(x), nchar(x))
    if (x == 1)
      i <- 1
    else if (x >= 2 && x <= 4)
      i <- 2
    else
      i <- 3
  }
  
  defs[[word]][i]
}