#' @export
..rename <- function(data, rstudio=TRUE) {
  assert_that(is.data.frame(data))
  assert_that(is.flag(rstudio))
  
  cols <- colnames(data)
  args <- paste0('`', cols, "`='", cols, "'")
  res <- formatFunctionCall('rename', args)
  
  if (rstudio) {
    .rstudio(res, indent=TRUE)
  } else {
    return(res)
  }
}