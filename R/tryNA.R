#' @export
#' @import plyr
tryNA <- function(expr) {
  try_default(expr, NA, quiet=TRUE)
}
