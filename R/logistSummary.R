#' 
#' @return 
#' @author Michal Burda
#' @export
logistSummary <- function(fit, thresh=0.05) {
  s <- summary(fit)
  tab <- cbind(coef(s), 
               `Odds Ratio` = exp(coef(fit)),
               exp(confint(fit)))
  tab <- tibble(variable = rownames(coef(s)),
                estimate = formatNumber(coef(s)[, 'Estimate'], digits = 2),
                `std.err.` = formatNumber(coef(s)[, 'Std. Error'], digits = 2),
                `OR` = formatNumber(exp(coef(s)[, 'Estimate']), digits = 2),
                `OR 95 % CI` = formatInterval(exp(confint(fit)[, 1]), exp(confint(fit)[,2]), digits = 2),
                `p-value` = formatPvalue(coef(s)[, 'Pr(>|z|)'], thresh=thresh))
  
  pander(tab,
         row.names = FALSE)
}
